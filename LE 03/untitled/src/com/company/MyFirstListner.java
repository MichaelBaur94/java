package com.company;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by mba on 16.05.2017.
 */
public class MyFirstListner implements MouseListener{

    JFrame parent;
    MyFirstListner(JFrame parent)
    {
        this.parent = parent;
    }


    @Override
    public void mouseClicked(MouseEvent e) {
        new MyFirstDialog(parent);

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
