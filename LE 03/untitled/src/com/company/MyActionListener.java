package com.company;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by mba on 16.05.2017.
 */
public class MyActionListener implements ActionListener
{
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() instanceof JComboBox)
            System.out.println(((JComboBox)e.getSource()).getSelectedItem());
    }
}
