package com.company;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.TreeSet;

/**
 * Created by mba on 16.05.2017.
 */
public class Aufgabe2 extends JFrame{

    Container _ContentPane;
    JLabel _label;

    public Aufgabe2() {
        super("Fenster - Dialoge...");
        this.setSize(450, 250);
        _ContentPane = getContentPane();
        TreeSet<String> set = new TreeSet<String>();
        set.add("Test1");
        set.add("Test2");
        set.add("Test3");
        System.out.print(set.toString());
        JComboBox jComboBox = new JComboBox(set.toArray());

        jComboBox.addActionListener(new MyActionListener());

/*
        jComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(e.getSource() instanceof JComboBox)
                    System.out.println(((JComboBox)e.getSource()).getSelectedItem());
            }
        });*/

        _ContentPane.add(jComboBox);

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
}
