package com.company;

import javax.swing.*;

/**
 * Created by mba on 16.05.2017.
 */
public class MyFirstDialog extends JDialog {
    public MyFirstDialog(JFrame parent)
    {
        super(parent,"MyFirstDialog");
        setSize(200,100);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setModal(true);
        setVisible(true);
    }
}
