package com.company;

/**
 * Created by mba on 16.05.2017.
 */
public class Person {
    private String vorname;
    private String nachname;


    public Person(String vorname, String nachname) {
        this.vorname = vorname;
        this.nachname = nachname;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(!(obj instanceof Person))
        {
            return false;
        }
        else
        {
            Person p = (Person)obj;
            return vorname.equals(p.getVorname()) && nachname.equals(p.getNachname());
        }
    }

    @Override
    public int hashCode()
    {
        return vorname.hashCode()*17 + nachname.hashCode();
    }

    @Override
    public String toString()
    {
        return vorname + " " +  nachname;
    }
}
