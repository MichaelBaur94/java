package com.company;

import javax.swing.*;
import java.awt.*;

/**
 * Created by mba on 19.05.2017.
 */
public class GridLayoutExample extends JFrame {
    public GridLayoutExample()
    {
        super("Example");
        setSize(500,500);
        setLocation(100,100);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        Container content = getContentPane();
        GridBagLayout gbl = new GridBagLayout();
        content.setLayout(gbl);

        JButton b1 = new JButton("Button 1");
        JButton b2 = new JButton("Button 2");
        JButton b3 = new JButton("Button 3");



        gbl.setConstraints(b1,createGridBagContrains(0,0,2,2,GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER));
        gbl.setConstraints(b2,createGridBagContrains(0,2,1,2,GridBagConstraints.NONE, GridBagConstraints.CENTER));
        gbl.setConstraints(b3,createGridBagContrains(1,2,1,1,GridBagConstraints.NONE, GridBagConstraints.CENTER));

        content.add(b1);
        content.add(b2);
        content.add(b3);
        setVisible(true);
    }



    private GridBagConstraints createGridBagContrains( int x, int y,int width , int height, int fill , int anchor)
    {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = x;
        gbc.gridy = y;

        gbc.ipadx = 100;
        gbc.ipady = 100;
        gbc.gridwidth = width;
        gbc.gridheight = height;
        gbc.fill = fill;
        gbc.anchor = anchor;

        gbc.weightx = 100;
        gbc.weighty = 100;
        return gbc;
    }


}
