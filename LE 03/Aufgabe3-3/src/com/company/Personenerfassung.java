package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by mba on 16.05.2017.
 */
public class Personenerfassung extends JFrame {

    Set<Person> personen;

    //Vorname
    private JLabel jLabelVorname;
    private JTextField jtextFieldVorname;

    //Nachname
    private JLabel jLabelNachname;
    private JTextField jtextFieldNachname;

    //Buttons
    private JButton jButtonOK;
    private JButton jButtonAbbrechen;

    //Menu
    private JMenuItem speichern;
    private JMenuItem beenden;
    private JMenu datei;

    public Personenerfassung() {
        super("Personenerfassung");
        this.setSize(400,300);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        personen = new HashSet<>();
        InitializeMenu();
        InitializeContentPane();

        getPersonen();
        setVisible(true);

    }

    private void deleteContent()
    {
        HsqldbDatabase db = new HsqldbDatabase();
        db.deleteContent();
        db.CloseDatabase();
    }

    private void getPersonen()
    {
        HsqldbDatabase db = new HsqldbDatabase();
        personen = db.getCurrentPersons();
        db.CloseDatabase();
    }

    private void InitializeMenu()
    {
        //Speichern
        speichern  = new JMenuItem("Speichern");
        KeyStroke keyStrokeSpeichern = KeyStroke.getKeyStroke('S', InputEvent.CTRL_MASK);
        speichern.setAccelerator(keyStrokeSpeichern);
        speichern.addActionListener(addPersonActionListner());

        //Benden
        beenden = new JMenuItem("Beenden");
        KeyStroke keyStrokeBeenden = KeyStroke.getKeyStroke('B', InputEvent.CTRL_MASK);
        beenden.setAccelerator(keyStrokeBeenden);
        beenden.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Beenden");
            }
        });

        //Datei
        datei = new JMenu("Datei");
        datei.add(speichern);
        datei.add(beenden);

        //Menübar
        JMenuBar jmBar = new JMenuBar();
        jmBar.add(datei);
        setJMenuBar(jmBar);
    }

    private void InitializeContentPane()
    {
        Container contentPane = getContentPane();

        //Vorname
        jLabelVorname = new JLabel("Vorname:");
        jtextFieldVorname = new JTextField(50);
        jtextFieldVorname.setName("Test");

        //Nachname
        jLabelNachname = new JLabel("Nachname:");
        jtextFieldNachname = new JTextField(50);

        //OK Button
        jButtonOK = new JButton("OK!");
        jButtonOK.addActionListener(addPersonActionListner());

        //Abbrechen Button
        jButtonAbbrechen = new JButton("Abbrechen");
        jButtonAbbrechen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false); //you can't see me!
                dispose(); //Destroy the JFrame object
            }
        });

        //Layout
        GridBagLayout gbl = new GridBagLayout();
        getContentPane().setLayout(gbl);

        jButtonOK.setMinimumSize(jButtonAbbrechen.getMinimumSize());

        addComponent(contentPane,gbl,jLabelVorname,createGridBagContrains(0,0,1,1,GridBagConstraints.NONE, GridBagConstraints.WEST));
        addComponent(contentPane,gbl,jtextFieldVorname,createGridBagContrains(1,0,2,1,GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER));
        addComponent(contentPane,gbl,jLabelNachname,createGridBagContrains(0,1,1,1,GridBagConstraints.NONE, GridBagConstraints.WEST));
        addComponent(contentPane,gbl,jtextFieldNachname,createGridBagContrains(1,1,2,1,GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER));
        addComponent(contentPane,gbl,jButtonOK,createGridBagContrains(1,2,1,1,GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER));
        addComponent(contentPane,gbl,jButtonAbbrechen,createGridBagContrains(2,2,1,1,GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER));
        pack();
    }

    private GridBagConstraints createGridBagContrains( int x, int y,int width , int height, int fill , int anchor)
    {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = x;
        gbc.gridy = y;
        gbc.insets = new Insets(10,10,10,10);
        gbc.gridwidth = width;
        gbc.gridheight = height;
        gbc.fill = fill;
        gbc.anchor = anchor;

        gbc.weightx = 1;
        gbc.weighty = 1;
        return gbc;
    }

    private void addComponent(Container container, GridBagLayout gbl, Component component , GridBagConstraints gbc)
    {
        gbl.setConstraints(component,gbc);
        container.add(component);
    }

    private ActionListener addPersonActionListner()
    {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addPerson();
            }
        };
    }

    private void addPerson() {
        if (jtextFieldNachname.getText().trim().length() > 0 && jtextFieldNachname.getText().trim().length() > 0) {
            Person a = new Person(jtextFieldVorname.getText().trim(), jtextFieldNachname.getText().trim());

            if(personen.add(a)) {
                HsqldbDatabase db = new HsqldbDatabase();
                db.createTableIfNotExists();
                db.savePerson(a);
                db.CloseDatabase();
            }
        }

        for (Person p : personen) {
            //System.out.println(p);
        }
    }
}