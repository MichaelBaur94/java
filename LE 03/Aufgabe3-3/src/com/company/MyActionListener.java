package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by mba on 17.05.2017.
 */
public class MyActionListener implements ActionListener {


    private Personenerfassung p;

    public MyActionListener(Personenerfassung p)
    {
        this.p = p;
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        Component[] c = p.getContentPane().getComponents();
        for(int i = 0; i < c.length ; i++)
        {
            if(c[i] != null) {
                if (c[i].getName() != null && c[i].getName().equals("Test")){
                    System.out.println(((JTextField) c[i]).getText());}
            }
        }
    }
}
