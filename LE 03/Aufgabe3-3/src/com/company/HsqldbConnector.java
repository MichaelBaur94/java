package com.company;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by mba on 19.05.2017.
 */
public class HsqldbConnector {

    private static Connection connection;
    public static Connection getConnection() throws ClassNotFoundException, SQLException {

        if(connection == null || connection.isClosed()) {
            //Treiber registieren
            Class.forName("org.hsqldb.jdbcDriver");

            //Verbindungsaufbau
            connection = DriverManager.getConnection("jdbc:hsqldb:file:C:\\temp\\hsqldb\\db", "sa", "");
            DatabaseMetaData dmd = connection.getMetaData();
            System.out.println("Verbindung hergestellt zu: " + dmd.getDatabaseProductName());
        }

        return connection;
    }

    private static void closeConnection() throws SQLException{
        connection.close();
    }
}
