package com.company;

import com.sun.org.apache.xalan.internal.xsltc.compiler.util.CompareGenerator;

/**
 * Created by mba on 09.05.2017.
 */
public class Buch implements Comparable<Buch> {
    private String autor;
    private String titel;
    private int erscheinungsjahr;

    public Buch(String autor, String titel, int erscheinungsjahr) {
        this.autor = autor;
        this.titel = titel;
        this.erscheinungsjahr = erscheinungsjahr;
    }

    public String toString()
    {
        return this.getAutor() + " " + this.getTitel() + " " + this.getErscheinungsjahr();
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public int getErscheinungsjahr() {
        return erscheinungsjahr;
    }

    public void setErscheinungsjahr(int erscheinungsjahr) {
        this.erscheinungsjahr = erscheinungsjahr;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        if(!(obj instanceof Buch))
            return false;

        Buch buch = (Buch) obj;
        return buch.autor.equals(this.autor) && buch.erscheinungsjahr == this.erscheinungsjahr && buch.titel.equals(this.titel);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 37* result + autor.hashCode();
        result = 37* result + titel.hashCode();
        result = 37* result + erscheinungsjahr;

        return result;
    }

    @Override
    public int compareTo(Buch o) {


        if(o.getAutor().equals(this.autor))
        {
            if(o.getTitel().equals(this.titel))
            {
             return  this.erscheinungsjahr - o.erscheinungsjahr;
            }
            else
            {
             return this.titel.compareTo(o.getTitel());
            }
        }
        else
            return this.autor.compareTo(o.getAutor());
    }
}
