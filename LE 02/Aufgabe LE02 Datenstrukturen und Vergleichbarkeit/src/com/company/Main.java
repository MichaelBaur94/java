package com.company;

import sun.reflect.generics.tree.Tree;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {






        Set<Buch> s = new TreeSet<>();

        for(int z = 0; z < 2; z++)
            for(int a = 0; a < 2 ; a++)
                for(int i = 0 ; i < 5 ; i++)
                    s.add(new Buch("Autor" + i , "Titel" + i+z, 2010+i + a));

        for (Buch b : s)
            System.out.println(b);


        System.out.println("Break");

        Set<Buch> s2 = new TreeSet<>(Comparator.comparing(Buch::getAutor).thenComparing(Buch::getErscheinungsjahr).thenComparing(Buch::getTitel));
        s2.addAll(s);

        Set<Buch> s4 = new TreeSet<Buch>(new BuchComparator());


        Comparator<Buch> c = Comparator
                .comparing(Buch::getAutor)
                .thenComparing(Buch::getTitel)
                .thenComparing(Buch::getErscheinungsjahr);

        for (Buch b : s2)
            System.out.println(b);



        /*  Ausgabe:

            Autor1 Titel1 2011
            Autor3 Titel3 2013
            Autor0 Titel0 2010
            Autor2 Titel2 2012
            Autor4 Titel4 2014
         */
    }
}
