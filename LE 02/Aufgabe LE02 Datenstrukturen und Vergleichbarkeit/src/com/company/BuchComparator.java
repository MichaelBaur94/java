package com.company;

import java.util.Comparator;

/**
 * Created by mba on 09.05.2017.
 */


/*
        Comperator

        Definieren einer Externen Ordnung
        Definition von außen
        Definieren von Ordnungen != Natürliche Ordnung


        -> Gleiche Logik wie bei Comparable

        Vorgehen
        1) Neue Klasse
        2) Implementieren von Comperator
        3) Implementieren der Methode compare(Objekt 1 , Objekt 2)
        4) Übergabe in Konstruktor

         */
public class BuchComparator implements Comparator<Buch> {


    @Override
    public int compare(Buch o1, Buch o2) {
        if(o2.getAutor().equals(o1.getAutor() ))
        {
            if(o2.getTitel().equals(o1.getTitel()))
            {
                return o2.getErscheinungsjahr() - o1.getErscheinungsjahr();
            }
            else
            {
                return o2.getTitel().compareTo(o1.getTitel());
            }
        }
        else
            return o2.getAutor().compareTo(o1.getAutor());
    }
}
