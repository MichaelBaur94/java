package com.company;

import java.util.Date;

/**
 * Created by mba on 09.05.2017.
 */
public class Student {
    private String vorname;
    private String nachname;
    private Date geburtsdatum;
    private int martikelnummer;

    public Student(String vorname, String nachname, Date geburtsdatum, int martikelnummer)
    {

        this.vorname = vorname;
        this.nachname = nachname;
        this.geburtsdatum = geburtsdatum;
        this.martikelnummer = martikelnummer;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public Date getGeburtsdatum() {
        return geburtsdatum;
    }

    public void setGeburtsdatum(Date geburtsdatum) {
        this.geburtsdatum = geburtsdatum;
    }

    /*
    Vertrag:
    Equals muss:
    reflexiv sein -> x.equals(x) => true;
    symmetrisch -> x.equals(y) = y.equals(x)
    transitiv -> x.equals(y) && y.equals(z) => x.equals(z)

    konsistent wird kein Attribut verändert, so bleibt dieser Wert gleich
     */

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        else if(!(obj instanceof  Student)) {
            return false;
        }

        Student s = (Student) obj;
        return s.geburtsdatum.equals(geburtsdatum) && s.vorname.equals(vorname) && s.nachname.equals(nachname) && s.martikelnummer == martikelnummer;
    }

    /*

    Hashcode muss für immer überschrieben werden wenn hashcode überschrieben wird

    Wenn 2 Objekte mit equals gleich sind, so sollte der Hashwert identisch sein

    Wenn sich 2 Objekte unterscheiden, so sollte ihr Hashwert unterschiedlich sein
     */

    @Override
    public int hashCode() {
        int result = 17;
        result += vorname.hashCode()
                + nachname.hashCode()
                + geburtsdatum.hashCode()
                + martikelnummer;

        return result;
    }

    public int getMartikelnummer() {
        return martikelnummer;
    }

    public void setMartikelnummer(int martikelnummer) {
        this.martikelnummer = martikelnummer;
    }
}
