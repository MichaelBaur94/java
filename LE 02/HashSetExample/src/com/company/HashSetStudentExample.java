package com.company;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by mba on 09.05.2017.
 */
public class HashSetStudentExample {

    public static void main(String[] args)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        Set<Student> s = new HashSet<>();



        try
        {
            Student stud1 = new Student("Michael","Mayer", sdf.parse("29.10.1992"), 1);
            Student stud2 = new Student("Marcel","Mayer", sdf.parse("29.10.1993"), 2);
            Student stud3 = new Student("Hans","Mayer", sdf.parse("29.10.1994"), 3);
            Student stud4 = new Student("Michael","Mayer", sdf.parse("29.10.1992"), 1);
            Student stud5 = new Student("Hans","Mayer", sdf.parse("29.10.1994"), 3);

            System.out.println(stud1.equals(stud4));

            s.add(stud1);
            s.add(stud2);
            s.add(stud3);
            s.add(stud4);
            s.add(stud5);
        }
        catch (ParseException e)
        {
            System.out.println(e);
        }


        for (Student stud : s) {
            System.out.println(stud + stud.getVorname());
        }
    }
}
