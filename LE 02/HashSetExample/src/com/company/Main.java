package com.company;

import java.util.HashSet;
import java.util.Set;

public class Main {

    public static void main(String[] args) {

	   Set<String> s = new HashSet<>();
	   s.add("Müller");
	   s.add("Mayer");
	   s.add("Schmidt");
	   s.add("Müller");
	   s.add("Schmidt");

        for (String name : s ) {
            System.out.println(name);
        }

    }
}
