package com.company;

import javax.swing.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        // write your code here



        HashSet<Eintrag> set = new HashSet<Eintrag>();
        set.add(new Eintrag("Beck", "123"));
        set.add(new Eintrag("Müller", "5123"));
        set.add(new Eintrag("Baur", "12312"));
        set.add(new Eintrag("Maier", "1235623"));
        set.add(new Eintrag("Mayer", "5134223"));
        set.add(new Eintrag("Meier", "33662"));
        set.add(new Eintrag("Müller", "2346"));
        set.add(new Eintrag("Müllermeister", "34542"));
        while(true)
        {
            try {
                String name = JOptionPane.showInputDialog("Bitte geben Sie einen Nachnamen ein:");

                if (name == null)
                    return;
                if(name.length() == 0)
                    continue;

                long t1 = System.nanoTime();
                Set<Eintrag> result = set.stream().filter(x -> x.name.toLowerCase().startsWith(name.toLowerCase())).collect(Collectors.toSet());
                long t2 = System.nanoTime();

                StringBuilder sb = new StringBuilder();
                for (Eintrag eintrag : result) {
                    sb.append("Name: " + eintrag.name + " | Telefonnummer: " + eintrag.telefonnummer  + "\n");
                }
                if(sb != null && sb.length() > 0)
                    JOptionPane.showMessageDialog(null,sb,"Ergebnisse für " + name , JOptionPane.INFORMATION_MESSAGE);
                else
                    JOptionPane.showMessageDialog(null,"Es wurden keine Ergebnisse gefunden","Ergebnisse für " + name,JOptionPane.INFORMATION_MESSAGE);
            }
            catch (Exception e){
                System.err.println("Unbekannter Fehler: " +  e.getMessage());
            }
        }
    }
}