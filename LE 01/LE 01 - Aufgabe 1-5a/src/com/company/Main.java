package com.company;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        ArrayList<Arbeitnehmer> list = new ArrayList<Arbeitnehmer>();
        list.add(new Arbeiter(20,20,20));
        list.add(new Manager(200,4000));
        list.add(new Mitarbeiter(20,20,20));

        DecimalFormat df = new DecimalFormat("#,##0.00");

        for(Arbeitnehmer arbeitnehmer : list)
        {
            System.out.println(df.format(Lohnbuchhaltung.lohnberechnung(arbeitnehmer)));
        }
    }
}
