package com.company;

/**
 * Created by mba on 18.04.2017.
 */
public class Arbeiter extends Arbeitnehmer {
    private double stundenlohn;
    private double ueberstundenzuschlag;
    private double schichtzulage;

    public Arbeiter(double stundenlohn, double ueberstundenzuschlag, double schichtzulage)
    {
        this.stundenlohn = stundenlohn;
        this.ueberstundenzuschlag = ueberstundenzuschlag;
        this.schichtzulage = schichtzulage;
    }
    @Override
    public double montatsBrutto() {
        return stundenlohn + ueberstundenzuschlag + schichtzulage ;
    }
}
