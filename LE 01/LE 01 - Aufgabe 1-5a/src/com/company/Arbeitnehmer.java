package com.company;

import java.util.Date;

/**
 * Created by mba on 18.04.2017.
 */
public abstract class Arbeitnehmer {
    public String name;
    public Date eintrittsdatum;

    public abstract double montatsBrutto();
}
