package com.company;

/**
 * Created by mba on 18.04.2017.
 */
public class Mitarbeiter extends Arbeitnehmer {

    private double grundgehalt;
    private double ortszuschlag;
    private double zulage;

    public Mitarbeiter(double zulage, double ortszuschlag, double grundgehalt)
    {
        this.zulage = zulage;
        this.ortszuschlag = ortszuschlag;
        this.grundgehalt = grundgehalt;
    }
    @Override
    public double montatsBrutto() {
        return grundgehalt + ortszuschlag + zulage ;
    }
}
