package com.company;

/**
 * Created by mba on 18.04.2017.
 */
public class Manager extends Arbeitnehmer {


    private double fixGehalt;
    private double umsatzbeteiligung;

    public Manager(double fixGehalt, double umsatzbeteiligung) {
        this.umsatzbeteiligung = umsatzbeteiligung;
        this.fixGehalt = fixGehalt;
    }


    public double montatsBrutto() {
        return umsatzbeteiligung + fixGehalt;
    }
}



