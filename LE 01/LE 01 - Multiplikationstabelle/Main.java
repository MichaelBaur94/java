package com.company;

public class Main {

    public static void main(String[] args) {
        int width = 200;
        int length = 100;
        int columnlength = getMaxLenght(width,length);

        createHeader(width,columnlength,length);
        createDataTable(width,length,columnlength);
    }

    private static void createHeader(int width, int columnlength,int length)
    {
        String header = formatResultWithWhiteSpaces("*" , getMaxLenght(length) , true) + " |";
        for (int i = 1 ; i <= width; i++)
        {
            header += formatResultWithWhiteSpaces(i,columnlength,false);
        }
        System.out.println(header);

        for(int i = 0 ; i <= header.length(); i++)
        {
            System.out.print("-");
        }

        System.out.println();
    }


    private static void createDataTable(int width, int length, int maxLength)
    {
        for(int rowCounter = 1; rowCounter <= length ; rowCounter++)
        {
            System.out.print(createLeftHeader(rowCounter,length));
            for(int columnCounter = 1; columnCounter <= width ; columnCounter++)
            {
                System.out.print(formatResultWithWhiteSpaces(columnCounter*rowCounter,maxLength,false));
            }
            System.out.println();
        }
    }

    private static String createLeftHeader(int number, int maxNumber)
    {
        return formatResultWithWhiteSpaces(number, getMaxLenght(maxNumber),true) + " |";
    }

    private static int getMaxLenght(int maxWidth, int maxLength)
    {
        return getMaxLenght(maxWidth*maxLength);
    }

    private static int getMaxLenght(int number)
    {
        return ("" + (number)).length();
    }

    private static String formatResultWithWhiteSpaces(String object,int maxLength,boolean leftHeader)
    {
        String result = object;
        while( result.length() < maxLength )
        {
            result = " " + result;
        }

        if(!leftHeader)
            result = "  " + result;
        return result;
    }

    private static String formatResultWithWhiteSpaces(int number,int maxLength,boolean leftHeader)
    {
        return formatResultWithWhiteSpaces("" + number,maxLength,leftHeader);
    }
}
