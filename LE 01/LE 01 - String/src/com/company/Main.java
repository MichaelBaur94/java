package com.company;

import java.time.LocalDateTime;

public class Main {

    public static void main(String[] args) {
        long start = System.nanoTime();
        makeXString(100000);
        long end = System.nanoTime();
        System.out.println(end-start);


        long start2 = System.nanoTime();
        makeXStringWithStringBuilder(100000000);
        long end2 = System.nanoTime();
        System.out.println(end2-start2);


    }

    public static String makeXStringWithStringBuilder(int anzahl)
    {
        StringBuilder sb = new StringBuilder();
        for(int i = 0 ; i < anzahl ; i++)
        {
            sb.append("X");
        }
        return sb.toString();
    }

    public static String makeXString(int anzahl)
    {
        String s = "";
        for(int i = 0 ; i < anzahl ; i++)
        {
           s += "X";
        }
        return s;
    }
}
