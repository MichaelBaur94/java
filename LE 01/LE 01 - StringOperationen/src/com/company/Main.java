package com.company;

import com.sun.org.apache.xpath.internal.operations.Bool;

import javax.swing.*;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
	// write your code here
        String input = JOptionPane.showInputDialog(null,"Bitte geben Sie einen Text ein:");

        String[] inputArray = fremdeFunktion(input);

        for(int i = 0 ; i < inputArray.length ; i++)
        {
            if(inputArray[i] != null)
                System.out.println(i + " → " + inputArray[i]);
        }
    }


    private static String[] eigeneFunktion(String input)
    {
        return input.split(";");
    }

    private static String[] fremdeFunktion(String input)
    {
        ArrayList<String> list = new ArrayList<String>();

        do {
            int indexOf = input.indexOf(";");
            if (indexOf != -1) {
                String substring = input.substring(0, indexOf);
                input = input.substring(indexOf + 1);

                if (substring != null && !substring.isEmpty()) {
                    list.add(substring);
                }
            }
        }
        while(input.contains(";"));
        list.add(input);
        return list.stream().toArray(String[]::new);
    }
}
