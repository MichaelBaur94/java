package com.company;
import javax.swing.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Main {

    public static void main(String[] args) {
        boolean eingabeErfolgreich = false;

        do {
            try {
                String eingabeString = JOptionPane.showInputDialog(null, "Bitte geben Sie Ihren Geburtstag ein.");
                if (eingabeString == null)
                    break;

                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                sdf.setLenient(false);
                Date birthday = sdf.parse(eingabeString);

                if(birthday.getTime() < new Date().getTime()){
                    eingabeErfolgreich = true;
                    getAge(birthday);
                    getRoundBirthdays(birthday);
                }
                else {
                    JOptionPane.showMessageDialog(null, "Ihr Geburtstag kann nicht in der Zukunft sein.", "Fehler", JOptionPane.ERROR_MESSAGE);
                }
            }
            catch (ParseException e) {
                JOptionPane.showMessageDialog(null, "Falsche Eingabe", "Fehler", JOptionPane.ERROR_MESSAGE);
            }
            catch (Exception e){
                JOptionPane.showMessageDialog(null, "Es ist ein unbekannter Fehler aufgetreten. " + e.getMessage(), "Fehler", JOptionPane.ERROR_MESSAGE);

                System.err.println(e.getMessage());
            }
        }
        while(!eingabeErfolgreich);
    }

    private static void getAge(Date birthday)
    {
        int result = 0;
        Calendar currentDateCalendar = Calendar.getInstance();
        Calendar birthdayCalendar = Calendar.getInstance();
        birthdayCalendar.setTime(birthday);

        currentDateCalendar.set(currentDateCalendar.get(Calendar.YEAR),currentDateCalendar.get(Calendar.MONTH),currentDateCalendar.get(Calendar.DAY_OF_MONTH),0,0,0);

        while(birthdayCalendar.getTimeInMillis() <= currentDateCalendar.getTimeInMillis() ) {
            birthdayCalendar.add(Calendar.YEAR,1);
            if(currentDateCalendar.getTimeInMillis() <= birthdayCalendar.getTimeInMillis()) {
                break;
            }
            result++;
        }
        JOptionPane.showMessageDialog(null,"Sie sind aktuell " + result + " Jahre alt.","Aktuelles Alter",JOptionPane.INFORMATION_MESSAGE);
    }

    private static void getRoundBirthdays(Date birthday)
    {
        Calendar birthdayCalendar = Calendar.getInstance();
        birthdayCalendar.setTime(birthday);
        StringBuilder sb = new StringBuilder();
        for(int i = 1; i <= 10 ; i++) {
            birthdayCalendar.add(Calendar.YEAR,10);
            sb.append(i* 10 + " Jahre am  " +  birthdayCalendar.get(Calendar.DAY_OF_MONTH) + "." + birthdayCalendar.get(Calendar.MONTH) + "." + birthdayCalendar.get(Calendar.YEAR) + "\n" );
        }
        JOptionPane.showMessageDialog(null,sb,"Runde Geburtstage",JOptionPane.INFORMATION_MESSAGE);
    }
}
