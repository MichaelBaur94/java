package com.company;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {

/*        String result = JOptionPane.showInputDialog("Willst du mit mir gehen?");
        if(result != null && result.equals("Ja"))
        {
            JOptionPane.showMessageDialog(null, ":)","Titel",JOptionPane.INFORMATION_MESSAGE);

        }
        else
        {
            JOptionPane.showMessageDialog(null, ":(","Titel",JOptionPane.INFORMATION_MESSAGE);
        }*/


        //Einschränken der Auswahl mit JOptionPane.YES_NO_CANCEL_OPTION für JA,NEIN,CANCEL
        //Einschränken der Auswahl mit JOptionPane.YES_NO_OPTION für JA,NEIN
        int result = JOptionPane.showConfirmDialog(null,"Möchten Sie wirklich Speichern?",
                "Titel",JOptionPane.YES_NO_OPTION);

        if(result == JOptionPane.YES_OPTION)
        {
            // Code, wenn ja ausgewählt wird
        }
        else if ( result == JOptionPane.YES_NO_OPTION)
        {
            //Code, wenn nein ausgewählt wird
        }
        else if(result == JOptionPane.CANCEL_OPTION)
        {
            //Code wenn abgebrochen wird
        }


    }
}
