package com.company;

import javax.swing.*;
import java.text.ParseException;
import java.time.Instant;

public class Main {

    public static void main(String[] args) {

        System.out.println(calculateFac(getNumber()));
    }

    private static int calculateFac(int number)
    {
        if(number < 0)
            return 0;

        int result = 1;
        for(int i = number ; i > 0 ; i--)
        {
            result*=i;
        }

        return result;
    }

    private static int getNumber()
    {
        try
        {
            String inputString = JOptionPane.showInputDialog("Bitte geben Sie eine Zahl ein:");
            if(inputString == null)
                return 0;
            return Integer.parseInt(inputString);
        }
        catch (NumberFormatException e) {
            return getNumber();
        }
    }
}
