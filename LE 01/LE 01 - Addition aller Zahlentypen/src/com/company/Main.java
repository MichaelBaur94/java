package com.company;

public class Main {
    public static void main(String[] args) {
        System.out.println(add(2, 3.4));
        System.out.println(add(1, 10));
        System.out.println(add(4.56788596858585858585, 0.1));
    }

    private static Number add(Number a, Number b)
    {
        return a.doubleValue()+b.doubleValue();
    }
}
