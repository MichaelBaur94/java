package com.company;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {

        boolean isDigit = false;
        do {
            try{
                String eingabeString = JOptionPane.showInputDialog(null,"Bitte eine Ganzzahl eingeben.");
                if(eingabeString == null)
                    break;

                int eingabe = Integer.parseInt(eingabeString);
                JOptionPane.showMessageDialog(null,"Ganzzahl:" + eingabe,"Information",JOptionPane.INFORMATION_MESSAGE);
                isDigit = true;
            }
            catch (NumberFormatException e)
            {
                int result = JOptionPane.showConfirmDialog(null,
                        "Falsche Eingabe. Möchten Sie eine erneute Eingabe durchführen?","Error",JOptionPane.YES_NO_OPTION);
                if(result != JOptionPane.YES_OPTION)
                {
                    break;
                }
            }
        }
        while(!isDigit);
   }
}
