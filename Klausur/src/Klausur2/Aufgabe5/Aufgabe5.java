package Klausur2.Aufgabe5;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by mba on 27.06.2017.
 */
public class Aufgabe5 {

    //Existiert laut Aufgabe -> Es müssen sich keine Gedanken um dieses Objekt gemacht werden
    private static Connection con;

    public void setBundesliagarangliste(String verein, int punkte) throws SQLException
    {
        PreparedStatement preparedStatement = con.prepareStatement("Insert INTO bundesliagarangliste (verein,punkte) VALUES (?,?);");
        preparedStatement.setString(1,verein);
        preparedStatement.setInt(2,punkte);
        preparedStatement.executeUpdate();
        con.commit();

        //close nach allen Updates -> höchstwahrscheinlich außerhalb
    }

}
