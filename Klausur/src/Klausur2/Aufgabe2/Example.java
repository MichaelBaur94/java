package Klausur2.Aufgabe2;

import java.text.ParseException;

/**
 * Created by mba on 27.06.2017.
 */
public class Example {
    public static void main(String[] args)
    {
        String s = "4711";
        System.out.println("Beginne mit 4711");
        System.out.println(umwandlung("4711"));

        System.out.println("Beginne mit 4711a");
        System.out.println(umwandlung("4711a"));
    }


    private static int umwandlung(String s)
    {
        try
        {
            //Umwandlung mit Datentyp (Integer) . parseInt(),
            return Integer.parseInt(s);

            //Fehler werden durch eine NumberFormatException, erkannt. Diese sollte behandelt werden
        }
        catch (NumberFormatException e)
        {
            System.out.print("Fehler mit String: ");
            System.out.println(s);
            return 0;
        }

    }
}
