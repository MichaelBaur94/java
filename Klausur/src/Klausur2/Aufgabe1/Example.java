package Klausur2.Aufgabe1;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by mba on 27.06.2017.
 */
public class Example {

    public static void main(String[] args)
    {
        //geiche elemente kommen nur einmal im Set vor
        //unsortiert

        String a = "Hallo";
        String b = "123";

        Set<String> set = new LinkedHashSet<>();
        set.add(a);
        set.add(a);
        set.add(b);
        set.add(b);
        set.add(b);

        for(String s : set) {
            System.out.println(s);
        }

        //Ausgabe 1x a und 1x b

    }
}
