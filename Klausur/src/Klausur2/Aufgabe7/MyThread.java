package Klausur2.Aufgabe7;

/**
 * Created by mba on 27.06.2017.
 */
public class MyThread extends Thread{

    @Override
    public void run()
    {
        while(true)
        {
            if(isInterrupted())
                break;

            System.out.println("Hallo Java!");
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                interrupt();
            }
        }
    }
}
