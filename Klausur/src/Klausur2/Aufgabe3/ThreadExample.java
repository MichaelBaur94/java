package Klausur2.Aufgabe3;

/**
 * Created by mba on 27.06.2017.
 */
public class ThreadExample extends Thread {
    @Override
    public void run() {
        int i = 0;
        while (i < 10) {
            if(interrupted())
                break;

            System.out.println(i);
            i++;
            try {

                sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
                interrupt();
            }
        }
    }
}
