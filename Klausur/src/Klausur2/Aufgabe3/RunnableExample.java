package Klausur2.Aufgabe3;

/**
 * Created by mba on 27.06.2017.
 */
public class RunnableExample implements Runnable {

    @Override public void run() {
        int i = 0;
        while (i < 10) {
            if(Thread.currentThread().isInterrupted())
                break;
            System.out.println(i);
            i++;
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
