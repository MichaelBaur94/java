package Klausur2.Aufgabe3;

/**
 * Created by mba on 27.06.2017.
 */
public class Example {


    public static void main(String[] args)
    {
        //Threads werden gerne beim Multithreading eingesetzt.
        //Bekannte Beispiele sind hier:
        //   - Aufwendige Prozesse werden in einem extra Thread ausgeführt, sodass die GUI nicht einfriert
        //   - Splash Screen -> während dem Laden des Programms wird uns eine Animation gezeigt

        //Mehere Sachen sollten aus performance gründen parallel ablaufen -> Server möchte mit merheren Clients gleichzeitig kommunizieren

        //Es könnten mehrere Ereignisketten gleichzeitig ausgeführt werden

        ThreadExample t = new ThreadExample();
        t.start();

        Thread t2 = new Thread(new RunnableExample());
        t2.start();
    }
}
