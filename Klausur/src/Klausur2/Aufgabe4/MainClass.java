package Klausur2.Aufgabe4;

import java.util.Comparator;
import java.util.TreeSet;

/**
 * Created by mba on 27.06.2017.
 */
public class MainClass {

    public static void main (String[] args)
    {
        Land l1 = new Land();
        l1.landesname = "A";
        l1.sprache = "2";

        Land l2 = new Land();
        l2.landesname = "A";
        l2.sprache = "1";

        Land l3 = new Land();
        l3.landesname = "A";
        l3.sprache = "2";

        Land l4 = new Land();
        l4.landesname = "B";
        l4.sprache = "2";

        Land l5 = new Land();
        l5.landesname = "B";
        l5.sprache = "1";

        Land l6 = new Land();
        l6.landesname = "C";
        l6.sprache = "1";

        TreeSet<Land> treeSet = new TreeSet<>();
        treeSet.add(l1);
        treeSet.add(l2);
        treeSet.add(l3);
        treeSet.add(l4);
        treeSet.add(l5);
        treeSet.add(l6);

        for(Land l : treeSet)
        {
            System.out.println(l.landesname + l.sprache);
        }

        System.out.println("---------------");
        TreeSet<Land> treeSet2 = new TreeSet<>( Comparator.comparing(Land::getLandesname).
                                                thenComparing(Land::getSprache));
        treeSet2.addAll(treeSet);

        for(Land l : treeSet2)
        {
            System.out.println(l.landesname + l.sprache);
        }

        System.out.println("---------------");

        TreeSet<Land> treeSet3 = new TreeSet<>(new LandComperator());
        treeSet3.addAll(treeSet);


        for(Land l : treeSet3)
        {
            System.out.println(l.landesname + l.sprache);
        }
    }
}
