package Klausur2.Aufgabe4;

/**
 * Created by mba on 27.06.2017.
 */
public class Land implements Comparable<Land>{
    public String landesname;
    public String sprache;

    public String getLandesname()
    {
        return landesname;
    }

    public String getSprache()
    {
        return sprache;
    }

    @Override
    public int compareTo(Land l) {
        if(landesname.equals(l.landesname))
        {
            if(sprache.equals(l.sprache))
            {
                return 0;
            }
            else
                return sprache.compareTo(l.sprache);
        }
        else
            return landesname.compareTo(l.landesname);
    }

    @Override
    public boolean equals(Object obj)
    {
        if(!(obj instanceof Land))
            return false;
        else
        {
            Land l = (Land) obj;
            return l.landesname.equals(landesname) && l.sprache.equals(sprache);
        }
    }
}
