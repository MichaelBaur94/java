package Klausur2.Aufgabe4;

import java.util.Comparator;

/**
 * Created by mba on 27.06.2017.
 */
public class LandComperator implements Comparator<Land> {

    @Override
    public int compare(Land o1, Land o2) {
        if(o1.landesname.equals(o2.landesname))
        {
            if(o1.sprache.equals(o2.sprache))
            {
                return 0;
            }
            else
                return o1.sprache.compareTo(o2.sprache);
        }
        else
            return o1.landesname.compareTo(o2.landesname);
    }
}
