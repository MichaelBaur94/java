package com.Klausur1.Aufgabe1;

import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;

/**
 * Created by mba on 27.06.2017.
 */
public class Example {
    public static void main(String[] args)
    {
        //Listen sind geordnet, Elemente können mehrfach vorkommen, es können Elemente an bestimmtens stellen hinzugefügt und ausgelesen werden

        //Hinzufügen ArrayList
        List<String> list = new ArrayList<String>();
        list.add("Test");
        list.add(1,"Test1");


        //Hinzufügen LinkedList
        List<String> list2 = new LinkedList<String>();
        list2.add("Test");
        list2.add(0,"Test1");


        //Bestimmte stelle abrufen
        System.out.println(list.get(1));


        //Allgemein Abrufen
        for(String item : list)
        {
            System.out.println(item);
        }

        for(String item : list2)
        {
            System.out.println(item);
        }

    }
}
