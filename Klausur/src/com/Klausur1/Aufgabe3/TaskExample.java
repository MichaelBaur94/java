package com.Klausur1.Aufgabe3;

/**
 * Created by mba on 27.06.2017.
 */
public class TaskExample {
    public String taskname;
    public int prioritaet;



    //Hash-Collections muss equals und hashCode überschrieben werden!

    @Override
    public int hashCode()
    {
        return taskname.hashCode() + prioritaet;

        //Bei mehreren Strings sollte ein Multiplikator mit einem Startwert verwendet werden

        /*
        int hashcode = 37;
        int multiplicator = 31;
        hashcode = hashcode * multiplicator + taskname.hashCode();
        hashcode = hashcode * multiplicator + andererString.hashCode();
        hashcode = hashcode * multiplicator + nochEinAndererString.hashCode();
        hashcode = hashcode * multiplicator + prioritaet;

        return hashcode;
        */
    }

    @Override
    public boolean equals(Object obj)
    {
        if(!(obj instanceof TaskExample))
        {
            return false;
        }
        TaskExample t = (TaskExample)obj;
        return t.prioritaet == this.prioritaet && t.taskname.equals(this.taskname);
    }
}
