package com.Klausur1.Aufgabe3;

import java.util.HashSet;

/**
 * Created by mba on 27.06.2017.
 */
public class MainClass {
    public static void main(String[] args)
    {
        //Erstellen Task 1
        TaskExample t = new TaskExample();
        t.taskname = "1";
        t.prioritaet = 2;

        //Erstellen Task 2
        TaskExample t1 = new TaskExample();
        t1.taskname = "2";
        t1.prioritaet = 3;

        //Wird nicht hinzugefügt, da es bereits existiert
        TaskExample t2 = new TaskExample();
        t2.taskname = "2";
        t2.prioritaet = 3;

        //Hinzufügen der Tasks zum Set
        HashSet<TaskExample> set = new HashSet<>();
        set.add(t);
        set.add(t1);
        set.add(t2);

        for (TaskExample a : set)
        {
            System.out.println(a.taskname);
        }
    }
}
