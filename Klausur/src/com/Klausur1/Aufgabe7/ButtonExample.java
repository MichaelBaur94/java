package com.Klausur1.Aufgabe7;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by mba on 27.06.2017.
 */
public class ButtonExample extends JFrame{

    public static void main(String[]args)
    {
        new ButtonExample();
    }

    public ButtonExample()
    {
        super("Titel");
        setSize(300,300);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JButton b = new JButton();

        // Buttons besitzen verschiedene Events die auf Ereignisse wie Click, MouseOver etc. reagieren.
        // Um mit diesen Events arbeiten zu können ist es notwendig einen Listener (eigene Klasse oder Inline) zu erstellen und diesen mit dem Button zu registrieren
        //
        // In unserem Fall ist es ein ActionListener, da wir auf den ButtonClick reagieren möchten.
        // Ich habe hierbei ein Objekt eines ActionListeners erzeugt und dem Button zugewiesen(registriert).
        // beim ActionListener wurde die Methode actionPerformed(ActionEvent e) überschrieben.
        // In dieser Funktion/Event kann der Programmierer seinen gewünschten Code schreiben, welcher ausgeführt werden soll, wenn der Button angeklickt worden ist.

        b.setActionCommand("Test");

        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println(e.getActionCommand());
            }
        });

        //Macht das selbe ; Jedoch keine Inline-Actionlistener
        b.addActionListener(new ListenerMBA());

        getContentPane().add(b);
        setVisible(true);
    }

}
