package com.Klausur1.Aufgabe4;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by mba on 27.06.2017.
 */

//gehört nicht direkt zur Aufgabe,
//kann jedoch trotzdem drankommen
//aber unwahrscheinlich

public class SQLConnector {

    private static Connection connection;

    public static Connection createConnection()
            throws ClassNotFoundException, SQLException {
        if(connection != null && !connection.isClosed())
        //1. Registrierung Treiber
        Class.forName("org.hsqldb.jdbcDriver");

        //2. Erstellung Connection Object:
        connection =  DriverManager.getConnection("jdbc:hsqldb:file:E:\\DB\\WI2013");

        return connection;
    }

    public static void closeConnection() throws SQLException {
        connection.close();
    }
}
