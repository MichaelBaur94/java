package com.Klausur1.Aufgabe4;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mba on 27.06.2017.
 */
public class DAO_Schneehoehe {

    //Auslesen
    public Map<String,Integer> schneehoehe(Connection con) throws SQLException
    {
        HashMap<String,Integer> hashMap = new HashMap<>();

        //Prüfen ob eine Verbindung vorhanden ist, wenn ja, dann soll sie auch nicht geschlossen sein
        if(con != null && !con.isClosed())
        {
            //Erstellen eines Statements
            Statement statement = con.createStatement();
            //Abfrage aus der Datenbank
            ResultSet resultSet = statement.executeQuery("SELECT * FROM schneehoehe");

            //Solange Datensatz vorhanden, füge der Map ein Datensatz hinzu
            while(resultSet.next())
            {
                hashMap.put(resultSet.getString("Ort"),resultSet.getInt("Schneehoehe"));
            }
        }
        return hashMap;
    }


    //GEHÖRT NICHT ZUR AUFGABE - KANN ABER TROTZDEM DRANKOMMEN

    //In Datenbank schreiben
    public void writeSchneehoehe(Connection con) throws SQLException
    {
        if(con != null && !con.isClosed())
        {
            PreparedStatement preparedStatement = con.prepareStatement("Insert INTO Schneehoehe (Ort,Schneehoehe) values (?,?);");
            preparedStatement.setString(1,"Name des Orts");
            preparedStatement.setInt(2,100); // 100 -> Schneehoehe

            preparedStatement.executeUpdate();
            con.commit();

            // Datenbank sollte geschlossen werden -> Wird normalerweise erst nach allen aktionen gemacht
        }
    }
}
