package com.Klausur1.Aufgabe8;

import java.util.Set;
import java.util.TreeSet;

public class Buch implements Comparable<Buch>{

    public String titel;
    public String autor;
    public int seitenanzahl;
    public double preis;


    public Buch(String titel, String autor, int seitenanzahl , double preis)
    {
        this.titel = titel;
        this.autor = autor;
        this.seitenanzahl = seitenanzahl;
        this.preis = preis;
    }

    @Override
    public int compareTo(Buch other)
    {
        //Wenn Preis gleich - N�chstes Kriterium
        if(preis == other.preis)
        {
            if(titel.equals(other.titel)) {
                //Nach Autor 3. Kriterium
                return autor.compareTo(other.autor);
            }

            //Nach Titel 2. Kriterium
            return titel.compareTo(other.titel);
        }

        //Nach Preis sortiert 1. Kriterium
        //Achtung wenn preisspanne zu klein ist, fliegt das 2. Element raus (Da beide den gleichen Preis nach dem Int-Cast haben);

        //Korrektur
        //if(preis > other.preis)
        //    return 1;
        //else
        //    return -1;

        return (int) (preis-other.preis);
    }

    public static void main(String[] args) {
        Buch b1 = new Buch("Harry Potter und der Stein der Weisen", "Joa",335,15.90);
        Buch b2 = new Buch("Java ist eine Insel", "Chri",1306,49.9);
        Buch b3 = new Buch("Die Herren", "Ge",335,16.99);
        Buch b4 = new Buch("Ill", "Dan",335,9.99);


        int a = (int) (b1.preis - b3.preis);
        System.out.println(a);

        Set<Buch> buecher = new TreeSet<>();
        buecher.add(b1);
        buecher.add(b2);
        buecher.add(b3);
        buecher.add(b4);
        buecher.add(b3);
        buecher.add(b1);

        for (Buch b: buecher) {
            System.out.println(b.titel);
        }


        //Ausgabe - Auch identische Ausgabe nach Korrektur (nach Preis)
        //Ill
        //Harry Potter und der Stein der Weisen
        //Die Herren
        //Java ist eine Insel




    }
}
