package com.Klausur1.Aufgabe2;

/**
 * Created by mba on 27.06.2017.
 */
public class Example {
    public static void main(String[] args)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("Durchläufe: ");

        //Hier sollte eigentich 1 000 000 stehen ; zur demo nur 100
        for(int counter = 0 ; counter < 100 ; counter++)
        {
            sb.append(" ");
            sb.append(counter);
        }

        System.out.println(sb.toString());
    }
}
