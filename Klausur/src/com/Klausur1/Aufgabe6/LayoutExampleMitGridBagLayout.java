package com.Klausur1.Aufgabe6;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;

/**
 * Created by mba on 27.06.2017.
 */
public class LayoutExampleMitGridBagLayout extends JFrame{

    public static void main(String[] args)
    {
        new LayoutExampleMitGridBagLayout();
    }

    public LayoutExampleMitGridBagLayout()
    {
        super("Titel");
        setSize(300,300);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        addMenu();

        //Komplexe Layouts können Mithilfe von Panels und verscheidenen Layouts dargestellt werden
        //Panel 1 Unterteilt, das Fenster z.B in 2 Teile mithilfe des Gridlayout´s
        //Im oberen Bereich wird z.b ein Button hinzugefügt - er erstreckt sich auf die komplette Fläche, falls nicht gewünscht extra Panel und eig. LayoutManager
        //Im unteren Bereich erzeugen wir ein Extra Panel mit einem anderen LayoutManager z.b Borderlayout. Hiermit können wir weitere Elemente hinzufügen z.b in der Mitte oder an einem Rand

        //P1 -> Element Oben und Unten
        //P2 -> wurde P1 unten hinzugefügt -> enthält weitere Panels oder Componenten.

        Container c = getContentPane();

        FlowLayout fl = new FlowLayout();

        BorderLayout bl = new BorderLayout();
        GridBagLayout gbl = new GridBagLayout();

        //Oben
        GridLayout gl = new GridLayout(2,1);
        JPanel p1 = new JPanel();
        p1.setLayout(gl);
        p1.add(getButton("Hallo"));

        //Unten
        JPanel p2 = new JPanel();
        p2.setLayout(bl);
        p2.add(getButton("Bla"),BorderLayout.EAST);

        JPanel p3 = new JPanel();
        p3.setLayout(gbl);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.gridheight = 1;
        gbc.gridwidth = 2;
        gbc.weightx = 100;
        gbc.weighty = 100;
        gbc.fill = GridBagConstraints.VERTICAL;
        gbc.anchor = GridBagConstraints.SOUTH;

        JButton b1 = getButton("GBL");
        gbl.setConstraints(b1,gbc);
        p3.add(b1);

        p2.add(p3,BorderLayout.CENTER);

        JPanel p4 = new JPanel();
        p4.setLayout(fl);
        p4.add(getButton("FlowLayout"));





        p2.add(p4,BorderLayout.SOUTH);






        p1.add(p2);


        getContentPane().add(p1);






        setVisible(true);
    }


    private void addMenu()
    {
        JMenuBar jmb = new JMenuBar();

        //Hauptelement
        JMenu jm = new JMenu("Datei");
        jm.setMnemonic('F');
        //Unterelement von Hauptelement
        JMenu jmi = new JMenu("item");

        //Unterelement des Unterelements
        JMenuItem jmi2 = new JMenuItem("item2");

        KeyStroke ksSave = KeyStroke.getKeyStroke('P', InputEvent.CTRL_MASK);
        jmi2.setAccelerator(ksSave);

        jmi.add(jmi2);
        jm.add(jmi);
        jmb.add(jm);

        setJMenuBar(jmb);


    }

    public JButton getButton(String name)
    {
        return new JButton(name);
    }

}
