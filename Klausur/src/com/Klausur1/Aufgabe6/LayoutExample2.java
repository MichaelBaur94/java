package com.Klausur1.Aufgabe6;

import javax.swing.*;
import java.awt.*;

/**
 * Created by mba on 28.06.2017.
 */
public class LayoutExample2 extends JFrame {
    public static void main(String[] args)
    {
        new LayoutExample2();
    }

    public LayoutExample2()
    {
        super("Hier könnte ein Titel stehen");
        setSize(400,400);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        //Komplexe Layouts können mit mehreren Panels, welche auch verschachtelt sein können, dargestellt werden.

        //Das Panel p1 wird durch den LayoutManager in Reihen gegliedert. Es kann also ein Element oben und ein Element unten platziert werden

        GridLayout gl = new GridLayout(2,1);
        Panel p1 = new Panel();
        p1.setLayout(gl);
        p1.add(new JButton("Ich bin Oben"));

        //Im unteren Teil von P1 möchten wir jedoch mehrere Elemente platzieren, oder sie unterschiedlich anordnen.
        //Daher wird ein zweites Panel erstellt
        //Dieses Panel wird in 2 Spalten aufgeteilt mithilfe des LayoutManagers

        GridLayout gl2 = new GridLayout(1,2);
        Panel p2 = new Panel();
        p2.setLayout(gl2);
        p2.add(new JButton("Ich bin unten Links"));


        // Unten Rechts soll der Button Im Norden angeordnet werden
        // Daher wird noch ein Panel mit dem Borderlayout erstellt

        BorderLayout bl = new BorderLayout();
        Panel p3 = new Panel();
        p3.setLayout(bl);
        p3.add(new JButton("Ich bin unten rechts - Im Norden"),BorderLayout.NORTH);

        p2.add(p3);
        p1.add(p2);


        //Oberstes Panel (p1) noch der ContentPane hinzufügen
        getContentPane().add(p1);

        setVisible(true);

    }
}
