package KlausurSonstiges.DateUndCalendar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by mba on 28.06.2017.
 */
public class Example {

    public static void main(String[] args)
    {
        String dateString = "28.06.2016";


        //Zum Parsen ein SimpleDateFormat erzeugen und im Konstruktor ( 'dd' für Tage , 'MM' für Monate MUSS GROß sein , und 'yyyy' für Jahre . dazwischen sollte euer Trennzeichen stehen also z.B. '.' oder '-'
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        try
        {
            //Umwandlung kann bei einem Falschen String eine ParseException auslösen
            // Wird z.B 32 für Tage übergeben so wird es bei 31 max. Tagen zum 1. und ein Monat addiert
            Date d = sdf.parse(dateString);
            getAlter(d);
            System.out.println(sdf.format(d));
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
    }


    private static void getAlter(Date d)
    {
        //Geburtsdatum
        //mit setTime wird dem Kalender das Gewünschte Datum übergeben
        Calendar birthday = Calendar.getInstance();
        birthday.setTime(d);

        //Heutiger Tag mit 0:00 Uhr
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR,0);
        c.set(Calendar.MINUTE,0);
        c.set(Calendar.SECOND,0);
        c.set(Calendar.MILLISECOND,0);

        //Geburtstag muss in Vergangenheit sein
        if(birthday.getTimeInMillis() <= c.getTimeInMillis())
        {
            //Jahresdifferenz ausrechnen und dem geburtstag addieren
            int age = c.get(Calendar.YEAR) - birthday.get(Calendar.YEAR);
            birthday.set(Calendar.YEAR,age);

            //Wenn Geburtstag Größer, dann hatte er bereits und ist ein Jahr älter geworden
            if(c.getTimeInMillis() <= birthday.getTimeInMillis())
                age++;

            System.out.println("Sie sind " + age + " Jahre alt");
        }

    }

}
