package KlausurSonstiges.Map;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mba on 28.06.2017.
 */
public class MapExample {

    public static void main(String[] args)
    {
        //Maps können sehr schnell auf den Schlüssel zugreifen und müssen dadurch nicht die kompletten Daten nach dem Schlüssel durchsuchen
        //wenn der Key die Telefonnummer und der Komplette Eintrag das Value ist. So kann anhand der Telefonnummer sehr schnell der Eintrag gefunden werden.
        //Die Telefonnummer kann nicht so schnell durch den Namen, welcher im Eintrag steht ermittelt werden.

        //Keys werden mit einem HashSet gespeichert
        //Values mit einer Liste


        Map<String,Integer> map = new HashMap<>();
        //hinzufügen
        map.put("String",1);



        //auslesen

        System.out.println(map.get("String"));

        //ODER
        for(Map.Entry<String,Integer> entry : map.entrySet())
        {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
        }

    }
}
